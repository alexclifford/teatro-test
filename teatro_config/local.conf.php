<?php

/*
 * Include any local instance specific configuration here - typically
 * this includes any database settings, email settings, etc that change
 * between installations.
 */

global $databaseConfig;
$databaseConfig = array(
    'type' => 'MySQLDatabase',
    'server' => '0.0.0.0',
    'username' => 'teatro',
    'password' => 'teatro',
    'database' => 'teatro',
);

if (!defined('SS_LOG_FILE')) {
    define('SS_LOG_FILE', '/apps/teatro/teatro_logs/silverstripe.log');
}

error_reporting(E_ALL);

ini_set('error_log', SS_LOG_FILE);
ini_set('display_errors', 'On');
ini_set('date.timezone', 'Australia/Melbourne');

// Set the Default Admin user
Security::setDefaultAdmin('devadmin2', 'devadmin2');

Director::set_environment_type('dev');

